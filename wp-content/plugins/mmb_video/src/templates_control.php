<?php 
/*
* ADD SCRIPT TO FOOTER 
*/
function mmb_add_footer_script() {
	
	wp_enqueue_style( 'mmb_style', CPT_PATH . 'css/main.min.css' );
    wp_enqueue_style( 'mmb_style_magnific', CPT_PATH . 'css/magnific-popup.css' );
    wp_enqueue_script( 'mmb_script_magnific', CPT_PATH  . 'js/jquery.magnific-popup.min.js' );
	wp_enqueue_script( 'mmb_script', CPT_PATH  . 'js/main.min.js' );
};
add_action( 'get_footer', 'mmb_add_footer_script' );





function videos_tpl_single($template){
  global $post;
  $found = locate_template('single-videos.php');
  if($post->post_type == 'videos'){
  	
    $single_template = dirname(MMB_PATH) . '/templates/single-videos.php';
  }

  return $single_template;
}
add_filter('single_template','videos_tpl_single');

/*
* REROUTE TO DEFAULT ARCHIVE TEMPLATE IF THERES NONE ON WP THEME
*/
function videos_tpl_archive($template){

  if(is_post_type_archive('videos')){
    $theme_files = array('archive-videos.php');
    $exists_in_theme = locate_template($theme_files, false);
    if($exists_in_theme == ''){
      return dirname(MMB_PATH) . '/templates/archive-videos.php';
    }
  }
  return $template;
}
add_filter('archive_template', 'videos_tpl_archive');




