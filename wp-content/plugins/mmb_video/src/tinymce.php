<?php


 function mmb_shortcode_button_init() {

      //Abort early if the user will never see TinyMCE
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;

      //Add a callback to regiser our tinymce plugin   
      add_filter("mce_external_plugins", "mmb_register_tinymce_plugin"); 

      // Add a callback to add our button to the TinyMCE toolbar
      add_filter('mce_buttons', 'mmb_add_tinymce_button');
}
 add_action('admin_init', 'mmb_shortcode_button_init');


//Register the plug-in
function mmb_register_tinymce_plugin($plugin_array) {
    $plugin_array['mmb_button'] = get_site_url().CPT_PATH.'js/shortcode.min.js';
    return $plugin_array;
}

//Add button to the toolbar
function mmb_add_tinymce_button($buttons) {
            //Add the button ID to the $button array
    $buttons[] = "mmb_button";
    return $buttons;
}
