## WordPress

Thanks for your interest in a wordpress developer role at MVF.

We have a simple challenge which we would like you to complete in your own time. You can spend as much or as little time on it as you wish but it would be great if you could get back to us within a week of receiving the instructions.


### Challenge

Create a custom plugin. Fork our repo and let us see your ideas! 


### What's the task? ###

* Create a custom plugin
* A plugin should create a custom post type called videos
* CPT must have these fields for admin: Title, Subtitle, Description, ID, and Type (options are Youtube, Vimeo and Dailymotion - dropdown or radio buttons)
* CPT should not have its single page for readers and should not be in menu navs for authors
* Create a shortcode that will display a video CPT with the following attributes:
```
#!php

[prefix_video id="POST ID" border_color="#3498db"]
```
* Border should be 8 pixels wide
* Video output should be responsive
* Layout should be responsive



### Optional ###

* Create shortcode generator - TinyMCE button that generates a popup with following fields: post id, width, and border color with color picker option



### Additional notes ###

* Plugin must be fully translatable
* Code must strictly follow the WP coding standards
* Code should not raise any warnings or notices

### The typical outcome might look something like this ###
![Desktop.png](https://bitbucket.org/repo/Ag6y9xy/images/3003731822-Desktop.png)# MVF Developer Test


---
### MVF
Do you want to work with the Smartest Tech and the Sharpest Minds? Apply at: http://www.mvfglobal.com/vacancies